/***************************************************************************************************
*   Filename:           global.h
*   Copyright:          (C)All Rights Reserved by Powervault Ltd 2017 
*
*   Title :             Global
*   Summary:            Global definitions
*
*   Target:             <todo Add Target Processor>
*   Compiler Name:      <todo Add Compiler Name>
*   Compiler Version:   <todo Add compiler Version.
*   Date Created:       18-Oct-17
*   Author:             Miodrag Stankovic
*
*   Notes:              <todo statement and may include other information 
*                       (eg. CM tool data, change history,dependencies, effects,requirements)>
*
****************************************************************************************************
* See Git Repo for Modification History 
***************************************************************************************************/

#ifndef GLOBAL_H
#define GLOBAL_H

/* ---------------------------------------    Includes      --------------------------------------*/
// System Includes 
#include <stdint.h>
#include <stdbool.h>

/* ------------------------------------        MACROS         ------------------------------------*/
// Macros
#ifndef NULL
#define NULL ( (void *) 0)   
#endif	

// Defines
/*------------------------------------------------------------------------------------------------*/
#define BIT0 	(0x00000001 << 0)
#define BIT1 	(0x00000001 << 1)
#define BIT2 	(0x00000001 << 2)
#define BIT3 	(0x00000001 << 3)
#define BIT4 	(0x00000001 << 4)
#define BIT5 	(0x00000001 << 5)
#define BIT6 	(0x00000001 << 6)
#define BIT7 	(0x00000001 << 7)
#define BIT8 	(0x00000001 << 8)
#define BIT9 	(0x00000001 << 9)
#define BIT10 (0x00000001 << 10)
#define BIT11 (0x00000001 << 11)
#define BIT12 (0x00000001 << 12)
#define BIT13 (0x00000001 << 13)
#define BIT14 (0x00000001 << 14)
#define BIT15 (0x00000001 << 15)
#define BIT16	(0x00000001 << 16)
#define BIT17	(0x00000001 << 17)
#define BIT18	(0x00000001 << 18)
#define BIT19	(0x00000001 << 19)
#define BIT20	(0x00000001 << 20)
#define BIT21	(0x00000001 << 21)
#define BIT22	(0x00000001 << 22)
#define BIT23	(0x00000001 << 23)
#define BIT24	(0x00000001 << 24)
#define BIT25	(0x00000001 << 25)
#define BIT26 (0x00000001 << 26)
#define BIT27 (0x00000001 << 27)
#define BIT28 (0x00000001 << 28)
#define BIT29 (0x00000001 << 29)
#define BIT30 (0x00000001 << 30)
#define BIT31 (0x00000001 << 31)

// Constants 
// None 

/* -----------------------------------        Typedefs         -----------------------------------*/
// Public enums

typedef enum 
{
	NC_ERROR = 128,     //Non-recoverable Error Occured during Execution of Function - ALREADY DEFINED! typedef enum {ERROR = 0, SUCCESS = !ERROR} ErrorStatus; 
	WARNING = 129,		//Recoverable Error Occured during Execution of Function 
	FAIL = 130,			//Function did not complete action
	PARTIAL = 131,		//Function partially completed action
	BAD_CRC = 132,		// CRC error
    BAD_PEC = 133,        
    SPI_FAIL = 134,     
    LTC6811_FAIL = 135,
    LTC8584_FAIL = 136,
    RS485_FAIL = 137,
    TX_FAIL = 138,
    RELAY_FAIL = 139,
    PRECHG_FAIL = 140, 
    I2C_WARNING = 141,
    I2C_FAIL = 142,
    ADDR_ERROR = 143,
    RS485_SBA_NO_POWER = 144,
    RS485_PACKET_OVERSIZE = 145,
    DEBUG_BUFFER_FULL = 146,
    DEBUG_BUFFER_EMPTY = 147,
	NONE = 0,			//No Status Info 
	OK = 1,				//Function completed action in full
    DEBUG_STARTED = 2,
    DEBUG_ENABLED = 3,
    RS485_INITIALISING = 4,
    RS485_SBA_GOOD = 5,
    RS485_RECEIVE_ENABLING = 6,
    RS485_TASK_NOT_OK = 7,
    RS485_TRANSMITTING = 8
}status_t;

// Public structures 

// Public variable types

/* -----------------------------------    Global Variables      ----------------------------------*/
// None 
/*Events*/
extern bool volatile interCoreTxMssgEvent; // Tx Message  to A7 Event
extern bool volatile getSensorDataEvent;   // Read Sensor Data Event

/***************************************************************************************************
****************************		  Function Prototypes	  	  ***********************************
***************************************************************************************************/
uint8_t     GetLsb(uint16_t i);
uint8_t     GetMsb(uint16_t i);
uint16_t    GetU16(uint8_t msb, uint8_t lsb);
bool        CompareBuffers(uint8_t* bufA, uint8_t* bufB, uint32_t length); 

#endif

/***************************************************************************************************
* 										END OF FILE												   																				 *
***************************************************************************************************/

