/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC16LF1519
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set nRESET aliases
#define nRESET_TRIS                 TRISAbits.TRISA7
#define nRESET_LAT                  LATAbits.LATA7
#define nRESET_PORT                 PORTAbits.RA7
#define nRESET_SetHigh()            do { LATAbits.LATA7 = 1; } while(0)
#define nRESET_SetLow()             do { LATAbits.LATA7 = 0; } while(0)
#define nRESET_Toggle()             do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define nRESET_GetValue()           PORTAbits.RA7
#define nRESET_SetDigitalInput()    do { TRISAbits.TRISA7 = 1; } while(0)
#define nRESET_SetDigitalOutput()   do { TRISAbits.TRISA7 = 0; } while(0)

// get/set ONOFF aliases
#define ONOFF_TRIS                 TRISBbits.TRISB0
#define ONOFF_LAT                  LATBbits.LATB0
#define ONOFF_PORT                 PORTBbits.RB0
#define ONOFF_WPU                  WPUBbits.WPUB0
#define ONOFF_ANS                  ANSELBbits.ANSB0
#define ONOFF_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define ONOFF_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define ONOFF_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define ONOFF_GetValue()           PORTBbits.RB0
#define ONOFF_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define ONOFF_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define ONOFF_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define ONOFF_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define ONOFF_SetAnalogMode()      do { ANSELBbits.ANSB0 = 1; } while(0)
#define ONOFF_SetDigitalMode()     do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set USB_RESET aliases
#define USB_RESET_TRIS                 TRISBbits.TRISB3
#define USB_RESET_LAT                  LATBbits.LATB3
#define USB_RESET_PORT                 PORTBbits.RB3
#define USB_RESET_WPU                  WPUBbits.WPUB3
#define USB_RESET_ANS                  ANSELBbits.ANSB3
#define USB_RESET_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define USB_RESET_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define USB_RESET_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define USB_RESET_GetValue()           PORTBbits.RB3
#define USB_RESET_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define USB_RESET_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define USB_RESET_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define USB_RESET_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define USB_RESET_SetAnalogMode()      do { ANSELBbits.ANSB3 = 1; } while(0)
#define USB_RESET_SetDigitalMode()     do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set SW9 aliases
#define SW9_TRIS                 TRISBbits.TRISB4
#define SW9_LAT                  LATBbits.LATB4
#define SW9_PORT                 PORTBbits.RB4
#define SW9_WPU                  WPUBbits.WPUB4
#define SW9_ANS                  ANSELBbits.ANSB4
#define SW9_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define SW9_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define SW9_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define SW9_GetValue()           PORTBbits.RB4
#define SW9_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define SW9_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define SW9_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define SW9_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define SW9_SetAnalogMode()      do { ANSELBbits.ANSB4 = 1; } while(0)
#define SW9_SetDigitalMode()     do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set SW8 aliases
#define SW8_TRIS                 TRISBbits.TRISB5
#define SW8_LAT                  LATBbits.LATB5
#define SW8_PORT                 PORTBbits.RB5
#define SW8_WPU                  WPUBbits.WPUB5
#define SW8_ANS                  ANSELBbits.ANSB5
#define SW8_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define SW8_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define SW8_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define SW8_GetValue()           PORTBbits.RB5
#define SW8_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define SW8_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define SW8_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define SW8_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define SW8_SetAnalogMode()      do { ANSELBbits.ANSB5 = 1; } while(0)
#define SW8_SetDigitalMode()     do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set LED4 aliases
#define LED4_TRIS                 TRISCbits.TRISC2
#define LED4_LAT                  LATCbits.LATC2
#define LED4_PORT                 PORTCbits.RC2
#define LED4_ANS                  ANSELCbits.ANSC2
#define LED4_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define LED4_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define LED4_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define LED4_GetValue()           PORTCbits.RC2
#define LED4_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define LED4_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define LED4_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define LED4_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set LED3 aliases
#define LED3_TRIS                 TRISCbits.TRISC3
#define LED3_LAT                  LATCbits.LATC3
#define LED3_PORT                 PORTCbits.RC3
#define LED3_ANS                  ANSELCbits.ANSC3
#define LED3_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define LED3_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define LED3_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define LED3_GetValue()           PORTCbits.RC3
#define LED3_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define LED3_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define LED3_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define LED3_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set LED2 aliases
#define LED2_TRIS                 TRISCbits.TRISC4
#define LED2_LAT                  LATCbits.LATC4
#define LED2_PORT                 PORTCbits.RC4
#define LED2_ANS                  ANSELCbits.ANSC4
#define LED2_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define LED2_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define LED2_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define LED2_GetValue()           PORTCbits.RC4
#define LED2_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define LED2_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define LED2_SetAnalogMode()      do { ANSELCbits.ANSC4 = 1; } while(0)
#define LED2_SetDigitalMode()     do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set LED1 aliases
#define LED1_TRIS                 TRISCbits.TRISC5
#define LED1_LAT                  LATCbits.LATC5
#define LED1_PORT                 PORTCbits.RC5
#define LED1_ANS                  ANSELCbits.ANSC5
#define LED1_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define LED1_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define LED1_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define LED1_GetValue()           PORTCbits.RC5
#define LED1_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define LED1_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define LED1_SetAnalogMode()      do { ANSELCbits.ANSC5 = 1; } while(0)
#define LED1_SetDigitalMode()     do { ANSELCbits.ANSC5 = 0; } while(0)

// get/set RC6 procedures
#define RC6_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define RC6_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define RC6_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define RC6_GetValue()              PORTCbits.RC6
#define RC6_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define RC6_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define RC6_SetAnalogMode()         do { ANSELCbits.ANSC6 = 1; } while(0)
#define RC6_SetDigitalMode()        do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set RC7 procedures
#define RC7_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define RC7_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define RC7_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define RC7_GetValue()              PORTCbits.RC7
#define RC7_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define RC7_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define RC7_SetAnalogMode()         do { ANSELCbits.ANSC7 = 1; } while(0)
#define RC7_SetDigitalMode()        do { ANSELCbits.ANSC7 = 0; } while(0)

// get/set EN_3V3 aliases
#define EN_3V3_TRIS                 TRISDbits.TRISD0
#define EN_3V3_LAT                  LATDbits.LATD0
#define EN_3V3_PORT                 PORTDbits.RD0
#define EN_3V3_ANS                  ANSELDbits.ANSD0
#define EN_3V3_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define EN_3V3_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define EN_3V3_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define EN_3V3_GetValue()           PORTDbits.RD0
#define EN_3V3_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define EN_3V3_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define EN_3V3_SetAnalogMode()      do { ANSELDbits.ANSD0 = 1; } while(0)
#define EN_3V3_SetDigitalMode()     do { ANSELDbits.ANSD0 = 0; } while(0)

// get/set EN_1V5 aliases
#define EN_1V5_TRIS                 TRISDbits.TRISD3
#define EN_1V5_LAT                  LATDbits.LATD3
#define EN_1V5_PORT                 PORTDbits.RD3
#define EN_1V5_ANS                  ANSELDbits.ANSD3
#define EN_1V5_SetHigh()            do { LATDbits.LATD3 = 1; } while(0)
#define EN_1V5_SetLow()             do { LATDbits.LATD3 = 0; } while(0)
#define EN_1V5_Toggle()             do { LATDbits.LATD3 = ~LATDbits.LATD3; } while(0)
#define EN_1V5_GetValue()           PORTDbits.RD3
#define EN_1V5_SetDigitalInput()    do { TRISDbits.TRISD3 = 1; } while(0)
#define EN_1V5_SetDigitalOutput()   do { TRISDbits.TRISD3 = 0; } while(0)
#define EN_1V5_SetAnalogMode()      do { ANSELDbits.ANSD3 = 1; } while(0)
#define EN_1V5_SetDigitalMode()     do { ANSELDbits.ANSD3 = 0; } while(0)

// get/set ON_MAX aliases
#define ON_MAX_TRIS                 TRISDbits.TRISD4
#define ON_MAX_LAT                  LATDbits.LATD4
#define ON_MAX_PORT                 PORTDbits.RD4
#define ON_MAX_ANS                  ANSELDbits.ANSD4
#define ON_MAX_SetHigh()            do { LATDbits.LATD4 = 1; } while(0)
#define ON_MAX_SetLow()             do { LATDbits.LATD4 = 0; } while(0)
#define ON_MAX_Toggle()             do { LATDbits.LATD4 = ~LATDbits.LATD4; } while(0)
#define ON_MAX_GetValue()           PORTDbits.RD4
#define ON_MAX_SetDigitalInput()    do { TRISDbits.TRISD4 = 1; } while(0)
#define ON_MAX_SetDigitalOutput()   do { TRISDbits.TRISD4 = 0; } while(0)
#define ON_MAX_SetAnalogMode()      do { ANSELDbits.ANSD4 = 1; } while(0)
#define ON_MAX_SetDigitalMode()     do { ANSELDbits.ANSD4 = 0; } while(0)

// get/set MAX_SKIP aliases
#define MAX_SKIP_TRIS                 TRISDbits.TRISD5
#define MAX_SKIP_LAT                  LATDbits.LATD5
#define MAX_SKIP_PORT                 PORTDbits.RD5
#define MAX_SKIP_ANS                  ANSELDbits.ANSD5
#define MAX_SKIP_SetHigh()            do { LATDbits.LATD5 = 1; } while(0)
#define MAX_SKIP_SetLow()             do { LATDbits.LATD5 = 0; } while(0)
#define MAX_SKIP_Toggle()             do { LATDbits.LATD5 = ~LATDbits.LATD5; } while(0)
#define MAX_SKIP_GetValue()           PORTDbits.RD5
#define MAX_SKIP_SetDigitalInput()    do { TRISDbits.TRISD5 = 1; } while(0)
#define MAX_SKIP_SetDigitalOutput()   do { TRISDbits.TRISD5 = 0; } while(0)
#define MAX_SKIP_SetAnalogMode()      do { ANSELDbits.ANSD5 = 1; } while(0)
#define MAX_SKIP_SetDigitalMode()     do { ANSELDbits.ANSD5 = 0; } while(0)

// get/set EV_5V aliases
#define EV_5V_TRIS                 TRISDbits.TRISD7
#define EV_5V_LAT                  LATDbits.LATD7
#define EV_5V_PORT                 PORTDbits.RD7
#define EV_5V_ANS                  ANSELDbits.ANSD7
#define EV_5V_SetHigh()            do { LATDbits.LATD7 = 1; } while(0)
#define EV_5V_SetLow()             do { LATDbits.LATD7 = 0; } while(0)
#define EV_5V_Toggle()             do { LATDbits.LATD7 = ~LATDbits.LATD7; } while(0)
#define EV_5V_GetValue()           PORTDbits.RD7
#define EV_5V_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define EV_5V_SetDigitalOutput()   do { TRISDbits.TRISD7 = 0; } while(0)
#define EV_5V_SetAnalogMode()      do { ANSELDbits.ANSD7 = 1; } while(0)
#define EV_5V_SetDigitalMode()     do { ANSELDbits.ANSD7 = 0; } while(0)

// get/set FLT aliases
#define FLT_TRIS                 TRISEbits.TRISE0
#define FLT_LAT                  LATEbits.LATE0
#define FLT_PORT                 PORTEbits.RE0
#define FLT_ANS                  ANSELEbits.ANSE0
#define FLT_SetHigh()            do { LATEbits.LATE0 = 1; } while(0)
#define FLT_SetLow()             do { LATEbits.LATE0 = 0; } while(0)
#define FLT_Toggle()             do { LATEbits.LATE0 = ~LATEbits.LATE0; } while(0)
#define FLT_GetValue()           PORTEbits.RE0
#define FLT_SetDigitalInput()    do { TRISEbits.TRISE0 = 1; } while(0)
#define FLT_SetDigitalOutput()   do { TRISEbits.TRISE0 = 0; } while(0)
#define FLT_SetAnalogMode()      do { ANSELEbits.ANSE0 = 1; } while(0)
#define FLT_SetDigitalMode()     do { ANSELEbits.ANSE0 = 0; } while(0)

// get/set DOK aliases
#define DOK_TRIS                 TRISEbits.TRISE1
#define DOK_LAT                  LATEbits.LATE1
#define DOK_PORT                 PORTEbits.RE1
#define DOK_ANS                  ANSELEbits.ANSE1
#define DOK_SetHigh()            do { LATEbits.LATE1 = 1; } while(0)
#define DOK_SetLow()             do { LATEbits.LATE1 = 0; } while(0)
#define DOK_Toggle()             do { LATEbits.LATE1 = ~LATEbits.LATE1; } while(0)
#define DOK_GetValue()           PORTEbits.RE1
#define DOK_SetDigitalInput()    do { TRISEbits.TRISE1 = 1; } while(0)
#define DOK_SetDigitalOutput()   do { TRISEbits.TRISE1 = 0; } while(0)
#define DOK_SetAnalogMode()      do { ANSELEbits.ANSE1 = 1; } while(0)
#define DOK_SetDigitalMode()     do { ANSELEbits.ANSE1 = 0; } while(0)

// get/set CHG aliases
#define CHG_TRIS                 TRISEbits.TRISE2
#define CHG_LAT                  LATEbits.LATE2
#define CHG_PORT                 PORTEbits.RE2
#define CHG_ANS                  ANSELEbits.ANSE2
#define CHG_SetHigh()            do { LATEbits.LATE2 = 1; } while(0)
#define CHG_SetLow()             do { LATEbits.LATE2 = 0; } while(0)
#define CHG_Toggle()             do { LATEbits.LATE2 = ~LATEbits.LATE2; } while(0)
#define CHG_GetValue()           PORTEbits.RE2
#define CHG_SetDigitalInput()    do { TRISEbits.TRISE2 = 1; } while(0)
#define CHG_SetDigitalOutput()   do { TRISEbits.TRISE2 = 0; } while(0)
#define CHG_SetAnalogMode()      do { ANSELEbits.ANSE2 = 1; } while(0)
#define CHG_SetDigitalMode()     do { ANSELEbits.ANSE2 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);


/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handler for the IOCBF4 pin functionality
 * @Example
    IOCBF4_ISR();
 */
void IOCBF4_ISR(void);

/**
  @Summary
    Interrupt Handler Setter for IOCBF4 pin interrupt-on-change functionality

  @Description
    Allows selecting an interrupt handler for IOCBF4 at application runtime
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    InterruptHandler function pointer.

  @Example
    PIN_MANAGER_Initialize();
    IOCBF4_SetInterruptHandler(MyInterruptHandler);

*/
void IOCBF4_SetInterruptHandler(void (* InterruptHandler)(void));

/**
  @Summary
    Dynamic Interrupt Handler for IOCBF4 pin

  @Description
    This is a dynamic interrupt handler to be used together with the IOCBF4_SetInterruptHandler() method.
    This handler is called every time the IOCBF4 ISR is executed and allows any function to be registered at runtime.
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    None.

  @Example
    PIN_MANAGER_Initialize();
    IOCBF4_SetInterruptHandler(IOCBF4_InterruptHandler);

*/
extern void (*IOCBF4_InterruptHandler)(void);

/**
  @Summary
    Default Interrupt Handler for IOCBF4 pin

  @Description
    This is a predefined interrupt handler to be used together with the IOCBF4_SetInterruptHandler() method.
    This handler is called every time the IOCBF4 ISR is executed. 
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    None.

  @Example
    PIN_MANAGER_Initialize();
    IOCBF4_SetInterruptHandler(IOCBF4_DefaultInterruptHandler);

*/
void IOCBF4_DefaultInterruptHandler(void);


/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handler for the IOCBF5 pin functionality
 * @Example
    IOCBF5_ISR();
 */
void IOCBF5_ISR(void);

/**
  @Summary
    Interrupt Handler Setter for IOCBF5 pin interrupt-on-change functionality

  @Description
    Allows selecting an interrupt handler for IOCBF5 at application runtime
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    InterruptHandler function pointer.

  @Example
    PIN_MANAGER_Initialize();
    IOCBF5_SetInterruptHandler(MyInterruptHandler);

*/
void IOCBF5_SetInterruptHandler(void (* InterruptHandler)(void));

/**
  @Summary
    Dynamic Interrupt Handler for IOCBF5 pin

  @Description
    This is a dynamic interrupt handler to be used together with the IOCBF5_SetInterruptHandler() method.
    This handler is called every time the IOCBF5 ISR is executed and allows any function to be registered at runtime.
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    None.

  @Example
    PIN_MANAGER_Initialize();
    IOCBF5_SetInterruptHandler(IOCBF5_InterruptHandler);

*/
extern void (*IOCBF5_InterruptHandler)(void);

/**
  @Summary
    Default Interrupt Handler for IOCBF5 pin

  @Description
    This is a predefined interrupt handler to be used together with the IOCBF5_SetInterruptHandler() method.
    This handler is called every time the IOCBF5 ISR is executed. 
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    None.

  @Example
    PIN_MANAGER_Initialize();
    IOCBF5_SetInterruptHandler(IOCBF5_DefaultInterruptHandler);

*/
void IOCBF5_DefaultInterruptHandler(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/