/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC16LF1519
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include <xc.h>
#include <string.h>
#include "mcc.h"
#include "crc16.h"

void control_sequence(void);
void EUSART_WriteS(const uint8_t *data, uint8_t len);

static bool sw_reset = false;
static bool power_on_reset = false;
static bool wdt_reset = false;
static bool rmclr_reset = false;
static bool bor_reset = false;

static uint8_t rxMsg[EUSART_RX_BUFFER_SIZE];
static bool sentAck = false;

bool sentAck_get(void)
{
    return sentAck;
}

void control_sequence(void)
{
    nRESET_SetLow();        // Hold reset on PICO module
    nRESET_SetDigitalOutput(); 
    __delay_ms(200);
    LED1_SetLow();          // LED1 OFF
    LED2_SetLow();          // LED2 OFF
    LED3_SetLow();          // LED3 OFF
    LED4_SetLow();          // LED4 OFF
    __delay_ms(200);
    EV_5V_SetHigh();        // EN5V ON
    __delay_ms(200);
    EN_1V5_SetHigh();       // EN1V5 ON
    __delay_ms(200);
    EN_3V3_SetHigh();       // EN3V3 ON
    __delay_ms(200);
    USB_RESET_SetHigh();    // USBRESET ON
    ON_MAX_SetHigh();       // ONMAX ON
    __delay_ms(200);
    nRESET_SetDigitalInput();       // Release reset on PICO module
}

/*
                         Main application
 */
void main(void)
{
    if (false == PCONbits.nPOR) {
        power_on_reset = true;
        PCONbits.nPOR = true;
    }
    if (false == PCONbits.nRWDT) {
        wdt_reset = true;
        PCONbits.nRWDT = true;
    }
    if (false == PCONbits.nRI) {
        sw_reset = true;
        PCONbits.nRI = true;
    }
    if (false == PCONbits.nRMCLR) {
        rmclr_reset = true;
        PCONbits.nRMCLR = true;
    }
    if (false == PCONbits.nBOR) {
        bor_reset = true;
        PCONbits.nBOR = true;
    }
    
    // initialize the device
    SYSTEM_Initialize();
    control_sequence();

    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();

//    for (int16_t i = 0; i < 600; i++) { // message-based watchdog starts after 10 mins
//        CLRWDT();
//        __delay_ms(1000);
//    }
    
    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    TMR2_StartTimer();   
    
    uint8_t rxIdx;
    uint8_t rxByte;
    
    while (true) {
        
        rxIdx = 0;
        
        if (EUSART_is_rx_ready()) {
            
            __delay_ms(20); // allow 230 bytes to be received at Baud Rate of 115200, long enough for whole probe message
            while(EUSART_is_rx_ready()) {
                rxByte = EUSART_Read();
                if(rxIdx < EUSART_TX_BUFFER_SIZE) {
                    rxMsg[rxIdx++] = rxByte;// cut off the remaining bytes
                }
            }

            if (rxIdx > 2 && 0 == calcCRC16(rxMsg, (uint16_t)rxIdx)) {
                if (!memcmp(rxMsg, "Powervault", (size_t)(rxIdx - 2))) {
                    watchdogTicked = true;
                    
                    if (power_on_reset) {
                        power_on_reset = false;
                        EUSART_WriteS("POR", 3);
                    }
                    else if (wdt_reset) {
                        wdt_reset = false;
                        EUSART_WriteS("WDT", 3);
                    }
                    else if (sw_reset) {
                        sw_reset = false;
                        EUSART_WriteS("SFT", 3);
                    }
                    else if (bor_reset) {
                        bor_reset = false;
                        EUSART_WriteS("BOR", 3);
                    }
                    else if (rmclr_reset) {
                        rmclr_reset = false;
                        EUSART_WriteS("MCR", 3);
                    }
                    else {
                        EUSART_WriteS("ACK", 3);
                    }
                    
                    sentAck = true;
                    __delay_ms(500); // prolong LED on-time to 500 ms in heartbeat
                    sentAck = false;
                }
                else {
                    EUSART_WriteS("NAK", 3);
                }
            }
            else {
                EUSART_WriteS("CRC", 3);
            }
        }
    }
}

/* Transmit a block of data */
void EUSART_WriteS(const uint8_t *data, uint8_t len)
{
    while (len > 0) {    
        EUSART_Write(*data++);
        len--;
    }
}


/**
 End of File
*/