/*
 * crc16.h
 *
 *  Created on: 14 March 2019
 *      Author: Weili
 */

#ifndef LIB_CRC16_H_
#define LIB_CRC16_H_

#include <stdio.h>
#include <stdlib.h>

#include "global.h"


/** MACROS **/ 

#define CRC_START_16  		(0x0000)
#define CRC_POLY_16			(0xA001)

/***GLOBALS***/

/*** Function Prototypes ***/

extern uint16_t calcCRC16(const uint8_t *pData , uint16_t sz);

#endif /* LIB_CRC16_H_ */
